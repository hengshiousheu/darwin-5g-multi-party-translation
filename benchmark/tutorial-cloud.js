import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
  ext: {
    loadimpact: {
      // Project: Default project
      projectID: 3668258,
      // Test runs with the same name groups test runs together.
      name: 'Test (07/11/2023-20:35:03)'
    }
  }
};

export default function () {
  http.get('https://test.k6.io');
  sleep(1);
}
