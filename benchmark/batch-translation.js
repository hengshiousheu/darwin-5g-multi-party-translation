import http from 'k6/http';
import { sleep, check, group } from 'k6';

// 设置测试的阶段
export let options = {
    stages: [
        { duration: '2m', target: 20 },  // 在2分钟内用户数增加到20
        { duration: '5m', target: 20 },  // 用户数保持在20持续5分钟
        { duration: '2m', target: 0 },   // 在2分钟内用户数减少到0
    ],
    thresholds: {
        http_req_duration: ['p(95)<500'],  // 95%的请求应在500毫秒内完成
    },
};


const languages = ["en", "ja", "ko", "zh"];
const inputs = [
    "Where is the bathroom?",
    "トイレはどこですか？",
    "화장실이 어디 있습니까?",
    "洗手间在哪里？"
];

function simulateScenario(inputText, targetLanguages) {
    const payload = JSON.stringify({
        input_text: inputText,
        target_languages: targetLanguages
    });

    const params = {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    };

    let response = http.post('http://localhost:8001/v1/batch-translate/', payload, params);
    
    check(response, {
        'is status 200': r => r.status === 200,
        'is translation correct': r => {
            let body = JSON.parse(r.body);
            return body.translations && Object.keys(body.translations).length === targetLanguages.length;
        },
    });
}

// 定义默认的测试函数
export default function () {

    // 场景一：同时多语言查询
    group('Simultaneous Multi-Language Queries', function () {
        // 对每种语言发起一个翻译请求
        for (let i = 0; i < languages.length; i++) {
            simulateScenario(inputs[i], languages);
        }
    });

    // 场景二：快速连续请求
    group('Rapid Successive Requests', function () {
        // 定义当前需要翻译的语言列表
        let currentLangs = [languages[0], languages[1]];
        simulateScenario(inputs[0], currentLangs); // 对第一种语言进行翻译
        sleep(1); // 休眠1秒模拟现实操作间隔
        simulateScenario(inputs[1], currentLangs); // 紧接着对第二种语言进行翻译
    });

    // 场景三：长时间演讲
    group('Long Duration Speech', function () {

        // 模拟一段长时间的演讲翻译请求
        simulateScenario("This is a long duration speech example, where a participant talks about various topics in a detailed manner.", [languages[2]]);
    });

    // 场景四：混合语言对话
    group('Mixed Language Dialogue', function () {
        // 模拟混合语言对话的场景
        simulateScenario(inputs[2], [languages[2], languages[3]]);
        sleep(1);
        simulateScenario(inputs[3], [languages[3], languages[0]]);
    });

    // 场景五：高背景噪音
    group('High Background Noise', function () {
        // 模拟在高噪音背景下的翻译请求
        simulateScenario(inputs[0], [languages[0]]);
    });

    // 场景六：专业或技术语言
    group('Technical or Industry-Specific Language', function () {
        // 模拟专业术语的翻译场景
        simulateScenario("Implementing advanced neural network architectures requires understanding both theoretical and practical aspects.", [languages[1]]);
    });

    // 场景七：低带宽或高延迟
    group('Low Bandwidth or High Latency', function () {
        // Simulate high latency
        sleep(2); // 增加2秒延迟来模拟低带宽或网络延迟
        simulateScenario(inputs[1], [languages[1]]);
    });

    sleep(1); // Simulate think time between groups
}
