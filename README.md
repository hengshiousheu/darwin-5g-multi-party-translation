# Madlad400-FastAPI

Madlad400-FastAPI 提供 Madlad400 系列模型进行翻译的服务。该项目利用 FastAPI 框架构建，支持通过 REST API 进行快速且高效的文本翻译。

## 特性

- **快速翻译**：支持多种语言快速翻译，确保翻译质量和速度。
- **自动设备选择**：根据运行环境自动选择最佳设备执行翻译任务（CPU/GPU）。
- **易于部署**：支持 Docker 容器化部署，简化部署流程。

## 快速开始

### 环境要求

- Python 3.8 或更高版本
- FastAPI
- Uvicorn 用于运行服务
- Docker（可选，用于容器化部署）

### 安装步骤

### 配置与环境变量
通过环境变量进行配置，支持以下环境变量：

- MADLAD400_MODEL_NAME: 使用的模型名称，默认为 "Heng666/madlad400-3b-mt-ct2-int8"。
- DEVICE: 指定运行设备，"auto"、"cpu" 或 "cuda"。
- CT2_USE_EXPERIMENTAL_PACKED_GEMM: 是否使用实验性的 GEMM 打包，默认为 "1"。

### 本地開發
```bash
./run_local.sh
```

### 本地開發（Docker 封裝）
```bash
DOCKER_BUILDKIT=1 docker buildx build --platform linux/amd64,linux/arm64 -t xiuxiumycena/xiuxiumycena/darwin-5g-multi-party-translation:0.0.1 . --push
```

開發階段可以使用，
backend 會直接 build . 來處理
```bash
docker-compose -f docker-compose-dev.yml up 
```

生產階段可以使用
backend 會採用 docker hug 上面 image 來處理
```bash
docker-compose -f docker-compose-prod.yml up 
```

## 壓力測試-K6
k6 是一個新興的工具，它使用 JavaScript 來編寫測試腳本，這對於熟悉 JavaScript 的前端開發者來說非常有吸引力。它可以通過 Homebrew 輕松安裝在 macOS 上，命令如下：brew install k6。

第一次執行者，可以執行以下指令用來確認是否建立 K6 環境
```
k6 run tutorial.js
```

(optional)如果想要配合 Grafana cloud k6 執行的話，可以執行以下指令
當然你的服務也要跑在公開雲上，無法使用 localhost 的服務進行測試
```
k6 cloud tutorial-cloud.js
```

測試本 API 指令，請執行以下內容

```
此為 翻譯
./k6 run \
--out 'dashboard=report=batch-translation-test-report.html&port=6556' \
--out json=batch-translation-test-result.json \
batch-translation.js
```

本地端跑測試 帶視覺化
安裝套件參考 [xk6-dashboard](https://github.com/grafana/xk6-dashboard)
```
./k6 run \
--out dashboard=report=test-report.html \
--out json=test-result.json \
tutorial.js
```