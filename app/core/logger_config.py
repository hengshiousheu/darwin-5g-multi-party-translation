# app/logger_config.py
from loguru import logger

def setup_logger():
    # 日志文件配置，包括文件位置、轮转大小、保留时间等
    logger.add(
        "runtime_logs.log", 
        rotation="10 MB", 
        retention="10 days", 
        level="INFO",
        format="{time} {level} {message}",
    )