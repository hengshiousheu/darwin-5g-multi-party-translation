from pydantic_settings import BaseSettings
from datetime import datetime

class Settings(BaseSettings):
    PROJECT_NAME: str = "達運5G多向翻譯系統-FastAPI"
    PROJECT_DESCRIPTION: str = "提供模型进行翻译"
    PROJECT_VERSION: str = "1.0.0"
    BUILD_DATE: str = datetime.now().strftime("%Y-%m-%d %H:%M:%S %Z")
    CONTACT: dict = {
        "name": "Heng-Shiou Sheu",
        "url": "https://huggingface.co/Heng666",
    }
    LICENSE_INFO: dict = {
        "name": "使用 MIT 许可证",
        "url": "https://opensource.org/licenses/MIT",
    }
    MADLAD400_MODEL_NAME: str = "Heng666/madlad400-3b-mt-ct2-int8"
    MADLAD400_SENTENCE_PIECE_NAME: str = "spiece.model"
    DEVICE: str = "auto"
    CT2_USE_EXPERIMENTAL_PACKED_GEMM: str = "1"

settings = Settings()
