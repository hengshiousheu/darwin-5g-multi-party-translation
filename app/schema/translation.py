from pydantic import BaseModel, Field
from typing import List, Dict

class TranslationRequest(BaseModel):
    input_text: str = Field(..., example="請問廁所在哪裡", description="要翻译的文本。")
    target_language: str = Field(..., example="ja", description="目标语言的代码。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th' 等。")

class TranslationResponse(BaseModel):
    translated_text: str

class BatchTranslationRequest(BaseModel):
    input_text: str = Field(..., example="請問廁所在哪裡", description="要翻译的文本。")
    target_languages: List[str] = Field(..., example=["ja", "en"], description="目标语言的代码列表。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th' 等。")

class BatchTranslationResponse(BaseModel):
    translated_text: Dict[str, str] = Field(..., description="包含所有目标语言及其对应翻译结果的字典。")