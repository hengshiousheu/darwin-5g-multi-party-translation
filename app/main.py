from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError, HTTPException
from starlette.exceptions import HTTPException as StarletteHTTPException
from app.api.router import api_router
from app.core.config import settings
from app.core.logger_config import setup_logger
from loguru import logger

# 配置 loguru 日志器
setup_logger()

app = FastAPI(
    title=settings.PROJECT_NAME,
    description=settings.PROJECT_DESCRIPTION,
    version=settings.PROJECT_VERSION,
    contact=settings.CONTACT,
    license_info=settings.LICENSE_INFO,
)

@app.on_event("startup")
async def startup_event():
    logger.info(f"{settings.PROJECT_NAME} is starting up.")

@app.on_event("shutdown")
async def shutdown_event():
    logger.info(f"{settings.PROJECT_NAME} is shutting down.")

logger.info("Adding CORS middleware")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"], #origins when origins is set
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    logger.error(f"Validation error: {exc}")
    return JSONResponse(status_code=400, content={"detail": exc.errors()})

@app.exception_handler(HTTPException)
async def http_exception_handler(request: Request, exc: HTTPException):
    logger.warning(f"HTTP exception: {exc}")
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.detail})

logger.info("Including routers")
app.include_router(api_router)

logger.info(f"{settings.PROJECT_NAME} started")
