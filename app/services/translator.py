import os
import ctranslate2
from sentencepiece import SentencePieceProcessor
from app.core.config import settings
from pathlib import Path
from huggingface_hub import snapshot_download, hf_hub_url, cached_download
import requests
from loguru import logger
from functools import lru_cache
from typing import List, Dict

def check_internet_connection():
    """检查是否有网络连接"""
    try:
        requests.get("https://huggingface.co", timeout=5)
        return True
    except requests.ConnectionError:
        logger.error("无法连接到 Hugging Face Hub，请检查你的网络连接。")
        return False

def download_model(model_name: str):
    """下载模型，如果模型已存在，则不重复下载"""
    try:
        if not check_internet_connection():
            raise ConnectionError("无法连接到 Hugging Face Hub，请检查你的网络连接。")
        
        # 构造模型文件的 Hugging Face Hub URL
        model_path = snapshot_download(model_name,resume_download=True)
        logger.info(f"模型 {model_name} 下载完成或从缓存中获取。")
        return model_path
    except Exception as e:
        logger.error(f"获取模型失败：{e}")
        raise ValueError(f"获取模型失败：{e}")

# 使用环境变量
os.environ["CT2_USE_EXPERIMENTAL_PACKED_GEMM"] = settings.CT2_USE_EXPERIMENTAL_PACKED_GEMM

# 检查并下载模型
model_path = download_model(settings.MADLAD400_MODEL_NAME)


tokenizer = SentencePieceProcessor()
tokenizer.load(f"{model_path}/{settings.MADLAD400_SENTENCE_PIECE_NAME}")
translator = ctranslate2.Translator(model_path, device=settings.DEVICE)
logger.info(f"當前執行 DEVICE 結果:{settings.DEVICE}")

## 添加處理換行符號問題
def handle_newlines(input_text, target_language):
    # 將輸入文本按換行符切割
    sentences = input_text.split('\n')

    # 過濾空句子並去除前後空白
    sentences = [s.strip() for s in sentences if s.strip()]

    # 將切割後的每個句子進行編碼
    encoded_sentences = [
        tokenizer.encode(f"<2{target_language}> {sentence}", out_type=str) for sentence in sentences
    ]
    
    return encoded_sentences

@lru_cache(maxsize=128)  # 缓存最多128个最近使用的翻译结果
def translate_text(input_text: str, target_language: str) -> str:
    logger.info(f"欲翻譯文字 {input_text} 目標語系 {target_language} ")
    tokens = [tokenizer.decode(i) for i in range(460)]
    lang_codes = [token[2:-1] for token in tokens if token.startswith("<2")]
    if target_language not in lang_codes:
        raise ValueError("Unsupported language code.")
    
    ## 添加處理換行符號問題
    # input_tokens = tokenizer.encode(f"<2{target_language}> {input_text}", out_type=str)
    # logger.info(f"tokenizer 結果:{input_tokens}")

    # 處理含有多個換行的文本
    encoded_sentences = handle_newlines(input_text, target_language)
    logger.info(f"tokenizer 結果:{encoded_sentences}")

    #@https://note.com/if001/n/n07faad488175
    results = translator.translate_batch(
        encoded_sentences, #[input_tokens], 
        batch_type="tokens", 
        beam_size=4, 
        sampling_topk=10,
        sampling_temperature=0.7,
        no_repeat_ngram_size=1,
        max_decoding_length=64,
        return_end_token=True,
        repetition_penalty=1.5
        )
    logger.info(f"results 結果:{results}")

    # 用於存儲所有翻譯結果的列表
    translated_sentences = []

    # 迭代處理每個翻譯結果
    for result in results:
        # 對每個翻譯結果的假設進行解碼
        for hypothesis in result.hypotheses:
            # 將 token 列表轉換為字符串
            sentence = tokenizer.decode(hypothesis)
            sentence = sentence.strip()
            translated_sentences.append(sentence)

    # translated_sentence = tokenizer.decode(results[0].hypotheses[0])
    # 將所有句子結合成一個單一的字符串
    combined_sentence = ''.join(translated_sentences)
    logger.info(f"翻譯結果 {combined_sentence}")
    return combined_sentence

def translate_multiple_languages(input_text: str, languages: List[str]) -> Dict[str, str]:
    """同时翻译多种语言"""
    results = {}

    for lang in languages:
        translated_text = translate_text(input_text, lang)
        results[lang] = translated_text

    return results