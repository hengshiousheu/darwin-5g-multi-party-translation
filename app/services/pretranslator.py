import os
from loguru import logger
from app.services.translator import translate_text as direct_translate
from app.core.config import settings

# 假设我们已经有了英文模型的配置信息
ENGLISH_INTERMEDIATE_LANG = "en"

def pretranslate_text(input_text: str, target_language: str) -> str:
    logger.info(f"[1/3]将原始文本先翻译成英文，再从英文翻译到目标语言：{target_language}")
    # 首先将文本翻译成英文
    intermediate_translation = direct_translate(input_text, ENGLISH_INTERMEDIATE_LANG)
    logger.info(f"[2/3]中间英文翻译结果：{intermediate_translation}")
    # 然后将英文翻译成目标语言
    final_translation = direct_translate(intermediate_translation, target_language)
    logger.info(f"[3/3]最终翻译结果：{final_translation}")
    return final_translation
