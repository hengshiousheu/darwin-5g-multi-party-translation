from fastapi import APIRouter, HTTPException
from app.schema.translation import BatchTranslationRequest, BatchTranslationResponse
from app.services.translator import translate_multiple_languages
from loguru import logger

router = APIRouter()

@router.post("/batch-translate/", response_model=BatchTranslationResponse)
async def translate(request: BatchTranslationRequest):
    """
    将文本从任何支持的语言翻译成另一种语言。

    - **input_text**: 想要翻译的文本。
    - **target_language**: 你想将文本翻译成的目标语言的 ISO 代码。支持的语言代码包括英语（en）、简体中文（zh）、繁体中文（zh_Hant）、日语（ja）、韩语（ko）、越南语（vi）、印尼语（id）、泰语（th）等。
    """
    try:
        # 调用翻译服务函数
        translated_sentence = translate_multiple_languages(request.input_text, request.target_languages)
    except ValueError as e:
        logger.error(f"Translation failed: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        logger.error(f"Unexpected error during translation: {str(e)}")
        raise HTTPException(status_code=500, detail=f"An internal error occurred: {str(e)}")
    
    return BatchTranslationResponse(translated_text=translated_sentence)
