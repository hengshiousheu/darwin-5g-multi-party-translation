from fastapi import APIRouter, HTTPException
from fastapi.responses import JSONResponse
from app.core.config import settings

root_router = APIRouter()

@root_router.get("/", response_class=JSONResponse)
async def root():
    content = {
        "message": "欢迎来到我的 FastAPI 应用！",
        "model_version": settings.MADLAD400_MODEL_NAME,
        "build_date": settings.BUILD_DATE,
        "project_version": settings.PROJECT_VERSION,
        "device": settings.DEVICE,
        "docs_url": "/docs",
        "redoc_url": "/redoc"
    }
    return JSONResponse(content=content)
