from fastapi import APIRouter

router = APIRouter()

@router.get("/languages/")
async def get_supported_languages():
    """
    获取翻译支持的语言列表。
    """
    return {
        "languages": [
            {"code": "en", "name": "英语"},
            {"code": "zh", "name": "简体中文"},
            {"code": "zh_Hant", "name": "繁体中文"},
            {"code": "ja", "name": "日语"},
            {"code": "ko", "name": "韩语"},
            {"code": "vi", "name": "越南语"},
            {"code": "id", "name": "印尼语"},
            {"code": "th", "name": "泰语"},
        ]
    }
