"""Endpoints."""
from fastapi import APIRouter, HTTPException
from app.schema.zero_shot_query import ZeroShotQueryTranslationRequest, ZeroShotQueryTranslationResponse
from app.services.pretranslator import pretranslate_text
from app.services.translator import translate_text as direct_translate
from opencc import OpenCC
import os

cc = OpenCC('s2tw')  # convert from Simplified Chinese to Traditional Chinese

# 從環境變數獲取截斷符號，如果沒有設置則使用預設值
def truncate_text(text: str) -> str:
    """
    對輸入文本進行預處理,包括:
    1. 移除句子結尾的指定字串(如"、""，""，。""。-""。 ")
    2. 可以通過環境變數自定義要移除的字串
    """

    symbols = os.getenv("TRUNCATION_SYMBOLS", "，。-|。 | ").split("|")
    # 檢查每個符號是否在文本中，然後截斷
    for symbol in symbols:
        index = text.find(symbol)
        if index != -1:
            return text[:index]
    return text

zero_shot_query_translation_router = APIRouter()

@zero_shot_query_translation_router.post(
        "/zero-shot-query", 
        description="Translate text from one language to another. Supported languages are: Traditional Chinese (zh), English (en), Japanese (ja), Korean (ko), Indonesian (id), Vietnamese (vi), Thai (th).", 
        response_model=ZeroShotQueryTranslationResponse
        )
async def zero_shot_query(request: ZeroShotQueryTranslationRequest):
    """
    将文本从任何支持的语言翻译成另一种语言。

    - **input_text**: 想要翻译的文本。
    - **target_language**: 你想将文本翻译成的目标语言的 ISO 代码。支持的语言代码包括英语（en）、简体中文（zh）、繁体中文（zh_Hant）、日语（ja）、韩语（ko）、越南语（vi）、印尼语（id）、泰语（th）等。
    """

    try:
        source_language = request.query.strip()
        translated_sentence = pretranslate_text(source_language, request.target_language)
        translated_sentence = truncate_text(translated_sentence)
        
        if request.source_language=='zh':
            source_language = cc.convert(source_language)
        if request.target_language=='zh':
            translated_sentence = cc.convert(translated_sentence)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    return ZeroShotQueryTranslationResponse(
        source_sentence=source_language,
        source_language=request.source_language,
        target_language=request.target_language,
        target_sentence=translated_sentence
    )