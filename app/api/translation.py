from fastapi import APIRouter, HTTPException
from app.schema.translation import TranslationRequest, TranslationResponse
from app.services.translator import translate_text

router = APIRouter()

@router.post("/translate/", response_model=TranslationResponse)
async def translate(request: TranslationRequest):
    """
    将文本从任何支持的语言翻译成另一种语言。

    - **input_text**: 想要翻译的文本。
    - **target_language**: 你想将文本翻译成的目标语言的 ISO 代码。支持的语言代码包括英语（en）、简体中文（zh）、繁体中文（zh_Hant）、日语（ja）、韩语（ko）、越南语（vi）、印尼语（id）、泰语（th）等。
    """
    try:
        translated_sentence = translate_text(request.input_text, request.target_language)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    
    return TranslationResponse(translated_text=translated_sentence)
