from fastapi import APIRouter
from app.api.translation import router
from app.api.pretranslation import router as pretranslation_router
from app.api.support_languages import router as support_languages_router
from app.api.usage_model import router as usage_model_router
from app.api.root import root_router  # 如果欢迎消息路由在另一个文件中定义
from app.api.zero_shot_query import zero_shot_query_translation_router
from app.api.batch_translation import router as batch_translation_router

api_router = APIRouter()
api_router.include_router(root_router)  # 添加欢迎消息路由，没有前缀
api_router.include_router(usage_model_router, prefix="/v1", tags=["config"])
api_router.include_router(support_languages_router, prefix="/v1", tags=["config"])
api_router.include_router(router, prefix="/v1", tags=["translation"])
api_router.include_router(zero_shot_query_translation_router, prefix="/api/translation", tags=["translation"])
api_router.include_router(pretranslation_router, prefix="/v1", tags=["pretranslation"])

api_router.include_router(batch_translation_router, prefix="/v1", tags=["batch"])
